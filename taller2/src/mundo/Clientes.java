package mundo;

import java.util.Date;

public class Clientes extends Trabajo {
	
	public enum DeNegocio{
		VENTAS,
		CONSULTA,
		CIRUGIA,
		URGENCIAS,
		
	}
	
	protected DeNegocio negocio;

	public Clientes(Date pFecha, String pLugar, boolean pObligatorio, boolean pFormal, TipoEventoTrabajo pTipo, DeNegocio pNegocio) {
		super(pFecha, pLugar, pObligatorio, pFormal, pTipo);
		// TODO Auto-generated constructor stub
		negocio = pNegocio;
	}
	
	public String conv (Boolean b)
	{
		if (!b) return "No";
		else
			return "Si";
	}
	
	public String toString(){
		return "Evento con CLIENTES: \n"
		  		+"FECHA: " + this.fecha
		  		+"\nLUGAR: " + this.lugar
		  		+"\nTIPO EVENTO: " + this.tipo
		  		+"\nOBLIGATORIO: " + conv(this.obligatorio)
		  		+"\nFORMAL: "+ conv(this.formal);
	}

}
