package mundo;

import java.util.Date;

public class Socios extends Trabajo {
	
	public enum Empresa{
		SUSALUD,
		LA_NUEVA_EPS,
		COLSANITAS,
		CAFAM,
		COOMPENSAR,
		
	}
	
	protected Empresa empresa;

	public Socios(Date pFecha, String pLugar, boolean pObligatorio,boolean pFormal, TipoEventoTrabajo pTipo, Empresa pEmpresa) {
		super(pFecha, pLugar, pObligatorio, pFormal, pTipo);
		// TODO Auto-generated constructor stub
		empresa = pEmpresa;
	}
	
	public String conv (Boolean b)
	{
		if (!b) return "No";
		else
			return "Si";
	}
	
	public String toString(){
		return "Evento con SOCIOS: \n"
		  		+"FECHA: " + this.fecha
		  		+"\nLUGAR: " + this.lugar
		  		+"\nTIPO EVENTO: " + this.tipo
		  		+"\nOBLIGATORIO: " + conv(this.obligatorio)
		  		+"\nFORMAL: "+ conv(this.formal);
	}

}
