package mundo;

import java.util.Date;

public class Trabajo extends Evento {
	
	public enum TipoEventoTrabajo{
		JUNTA,
		ALMUERZO,
		CONFERENCIA,
		COCTELES,
		FIESTA,
		OTRA_ACTIVIDAD_DE_TRABAJO,
		
		
	}
	
	protected TipoEventoTrabajo tipo;

	public Trabajo(Date pFecha, String pLugar, boolean pObligatorio,boolean pFormal, TipoEventoTrabajo pTipo) {
		super(pFecha, pLugar, pObligatorio, pFormal);
		tipo = pTipo;
	}

}
