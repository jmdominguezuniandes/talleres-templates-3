package taller.test;

import taller.mundo.AlgorithmTournament.TipoOrdenamiento;
import taller.mundo.teams.BubbleSortTeam;
import junit.framework.TestCase;

public class BubbleSortTest extends TestCase 
{
	
	private BubbleSortTeam equipoBurbuja;
	
	public Integer[] setupEscenario1(){
		equipoBurbuja = new BubbleSortTeam();
		Integer[] lista = new Integer[8];
		lista[0] = 11;
		lista[1] = 8;
		lista[2] = 3;
		lista[3] = 17;
		lista[4] = 1;
		lista[5] = 100;
		lista[6] = 6;
		lista[7] = 52;
		return lista;
	}
	
	public void testBubbleSort()
	{
		Integer[] lista = setupEscenario1();
		equipoBurbuja.sort(lista, TipoOrdenamiento.ASCENDENTE);
		assertEquals("El primer elemento debería ser 1", 1, (int) lista[0]);
		assertEquals("El segundo elemento debería ser 3", 3, (int) lista[1]);
		assertEquals("El tercer elemento debería ser 6", 6, (int) lista[2]);
		assertEquals("El cuarto elemento debería ser 8", 8, (int) lista[3]);
		assertEquals("El quinto elemento debería ser 11", 11, (int) lista[4]);
		assertEquals("El sexto elemento debería ser 17", 17, (int) lista[5]);
		assertEquals("El septimo elemento debería ser 52", 52, (int) lista[6]);
		assertEquals("El octavo elemento debería ser 100", 100, (int) lista[7]) ;
		
		lista = setupEscenario1();
		equipoBurbuja.sort(lista, TipoOrdenamiento.DESCENDENTE);
		assertEquals("El primer elemento debería ser 100", 100, (int) lista[0]);
		assertEquals("El segundo elemento debería ser 52", 52, (int) lista[1]);
		assertEquals("El tercer elemento debería ser 17", 17, (int) lista[2]);
		assertEquals("El cuarto elemento debería ser 11", 11, (int) lista[3]);
		assertEquals("El quinto elemento debería ser 8", 8, (int) lista[4]);
		assertEquals("El sexto elemento debería ser 6", 6, (int) lista[5]);
		assertEquals("El septimo elemento debería ser 3", 3, (int) lista[6]);
		assertEquals("El octavo elemento debería ser 1", 1, (int) lista[7]) ;
	}

}
