package taller.mundo.teams;

/*
 * SelectionSortTeam.java
 * This file is part of AlgorithmRace
 *
 * Copyright (C) 2015 - ISIS1206 Team 
 *
 * AlgorithmRace is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * AlgorithmRace is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AlgorithmRace. If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.Arrays;
import static taller.mundo.AlgorithmTournament.TipoOrdenamiento;

public class SelectionSortTeam extends AlgorithmTeam
{
     public SelectionSortTeam()
     {
          super("Selection Sort (-)");
          userDefined = false;
     }

     @Override
     public Comparable[] sort(Comparable[] list, TipoOrdenamiento orden)
     {
          return selectionSort(list, orden);
     }

     /**
      Ordena un arreglo de enteros, usando Ordenamiento por selección.
      @param arr Arreglo de enteros.
    **/
    private Comparable[] selectionSort(Comparable[] arr, TipoOrdenamiento orden)
    {
    	// Para hacer en casa
    	if(orden.equals(TipoOrdenamiento.ASCENDENTE)){
    		int N = arr.length;
        	for(int i = 0; i < N; i++){
        		int min = i;
        		for ( int j = i + 1; j < N ; j++){
        			if(less(arr[j], arr[min])){
        				min = j;
        			}
        			
        		}
        		exch (arr, i, min);
        		
        	}
    	}
    	
    	else if (orden.equals(TipoOrdenamiento.DESCENDENTE)){
    		int N = arr.length;
        	for(int i = 0; i < N; i++){
        		int max = i;
        		for ( int j = i + 1; j < N ; j++){
        			if(max(arr[j], arr[max])){
        				max = j;
        			}
        			
        		}
        		exch (arr, i, max);
        		
        	}
    	}
    	
    	return arr;
    }
    
    private boolean less (Comparable v, Comparable w){
    	return v.compareTo(w) < 0;
    }
    
    private boolean max(Comparable v, Comparable w){
    	return v.compareTo(w) >= 0;
    }
    
    private void exch(Comparable arr[], int i, int j){
    	Comparable temp = arr[i];
    	arr[i] = arr[j];
    	arr[j] = temp;
    }
}
