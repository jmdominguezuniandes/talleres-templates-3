package taller.mundo.teams;

/*
 * QuickSortTeam.java
 * This file is part of AlgorithmRace
 *
 * Copyright (C) 2015 - ISIS1206 Team 
 *
 * AlgorithmRace is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * AlgorithmRace is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AlgorithmRace. If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.Arrays;
import java.util.Random;
import static taller.mundo.AlgorithmTournament.TipoOrdenamiento;

public class QuickSortTeam extends AlgorithmTeam
{

     private static Random random = new Random();

     public QuickSortTeam()
     {
          super("Quicksort (*)");
          userDefined = true;
     }

     @Override
     public Comparable[] sort(Comparable[] list, TipoOrdenamiento orden)
     {
          quicksort(list, 0, list.length - 1, orden);
          return list;
     }
        // Trabajo en Clase
     
     private static void quicksort(Comparable[] lista, int inicio, int fin, TipoOrdenamiento orden)
     {
    	 // Trabajo en Clase
    	 if(fin <= inicio) return;
    	 int j = particion(lista, inicio, fin, orden);
    	 quicksort(lista, inicio, j-1, orden);
    	 quicksort(lista, j+1, fin, orden);
     }

    private static int particion(Comparable[] lista, int inicio, int fin, TipoOrdenamiento orden)
    {
    	// Trabajo en Clase
    	if(orden.equals(TipoOrdenamiento.ASCENDENTE)){
    		int i = inicio;
    		int j = fin + 1;
    		Comparable p = lista[inicio];
    		while(true){
    			while (less(lista[++i], p)){
    				if (i == fin){
    					break;
    				}
    			}
    			while(less(p, lista[--j])){
    				if(j == inicio){
    					break;
    				}
    			}
    			if(i >= j){
    				break;
    			}
    			exch(lista, i, j);
    		}
    		exch(lista, inicio, j);
    		return j;
    	}
    	else {
    		int i = inicio;
    		int j = fin + 1;
    		Comparable p = lista[inicio];
    		while(true){
    			while (max(lista[++i], p)){
    				if (i == fin){
    					break;
    				}
    			}
    			while(max(p, lista[--j])){
    				if(j == inicio){
    					break;
    				}
    			}
    			if(i >= j){
    				break;
    			}
    			exch(lista, i, j);
    		}
    		exch(lista, inicio, j);
    		return j;
    	}
    	
    	
    }

    private static int eleccionPivote(int inicio, int fin)
    {
         /**
           Este procedimiento realiza la elecci�n de un �ndice que corresponde al pivote res-
           pecto al cual se realizar�  la partici�n de la lista. Se recomienda escoger el ele-
           mento que se encuentra en la mitad, o de forma aleatoria entre los �ndices [inicio, fin).
         **/
    	// Trabajo en Clase
         return 0;
    }

    /**
      Retorna un número aleatorio que se encuentra en el intervalo [min, max]; inclusivo.
      @param min, índice inicial del intervalo.
      @param max, índice final del intervalo.
      @return Un número aleatorio en el intervalo [min, max].
    **/
    public static int randInt(int min, int max) 
    {
          int randomNum = random.nextInt((max - min) + 1) + min;
          return randomNum;
    }
    // Trabajo en Clase
    
    public static boolean less(Comparable v, Comparable w){
    	return v.compareTo(w) < 0;
    }
    public static boolean max(Comparable v, Comparable w){
    	return v.compareTo(w) >= 0;
    }
    
    public static void exch(Comparable[] lista, int i, int j){
    	Comparable temp = lista[i];
    	lista[i] = lista[j];
    	lista[j] = temp;
    }

}
