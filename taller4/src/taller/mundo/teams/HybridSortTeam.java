package taller.mundo.teams;

import java.util.Arrays;

import taller.mundo.AlgorithmTournament.TipoOrdenamiento;

public class HybridSortTeam extends AlgorithmTeam{

	public HybridSortTeam() {
		super("Hybrid sort (-)");
		userDefined = false;
		// TODO Auto-generated constructor stub
	}

	@Override
	public Comparable[] sort(Comparable[] list, TipoOrdenamiento orden) {
		// TODO Auto-generated method stub
		if(orden.equals(TipoOrdenamiento.ASCENDENTE)){
			int j = partirLista(0, list.length);
			Comparable aux1[] = new Comparable[j];
			Comparable aux2[] = new Comparable[list.length - j];
			for(int i = 0; i < j ; i++){
				aux1[i] = list[i];
			}
			for(int i = j; i < list.length; i++){
				aux2[i - j] = list[i];
			}
			insertionSort(aux1, orden);
			insertionSort(aux2, orden);
			
			for(int i = 0; i < aux1.length; i++){
				list[i] = aux1[i];
			}
			for(int i = (list.length - aux1.length); i < aux2.length; i++){
				list[i] = aux2[i];
			}
			
			quicksort(list, 0, list.length - 1, orden);
			
		}
		else if(orden.equals(TipoOrdenamiento.DESCENDENTE)){
			int j = partirLista(0, list.length);
			Comparable aux1[] = new Comparable[j];
			Comparable aux2[] = new Comparable[list.length - j];
			for(int i = 0; i < j ; i++){
				aux1[i] = list[i];
			}
			for(int i = j; i < list.length; i++){
				aux2[i - j] = list[i];
			}
			insertionSort(aux1, orden);
			insertionSort(aux2, orden);
			
			for(int i = 0; i < aux1.length; i++){
				list[i] = aux1[i];
			}
			for(int i = (list.length - aux1.length); i < aux2.length; i++){
				list[i] = aux2[i];
			}
			quicksort(list,0,list.length -1, orden);
		}
		return list;
	}
	
	private boolean less(Comparable v, Comparable w){
		return v.compareTo(w) < 0;
	}
	private boolean max(Comparable v, Comparable w){
		return v.compareTo(w) >= 0;
	}
	
	private void exch (Comparable[] lista, int i, int j){
		Comparable temp = lista[i];
		lista[i] = lista[j];
		lista[j] = temp;
	}
	
	private int partirLista(int inicio, int fin){
		int mitad = (int) (inicio + fin)/2;
		return mitad;
	}
	
	   private Comparable[] insertionSort(Comparable[] arr, TipoOrdenamiento orden)
	   {
		    // Para hacer en casa
		   if (orden.equals(TipoOrdenamiento.ASCENDENTE)){
			   int N = arr.length;
			   for(int i = 1; i < N ; i++){
				   for(int j = i; j > 0 && less(arr[j], arr[j-1]); j--){
					   exch(arr,j,j-1);
				   }
			   }
		   }

		   else if (orden.equals(TipoOrdenamiento.DESCENDENTE)){
			   int N = arr.length;
			   for(int i = 1; i < N ; i++){
				   for(int j = i; j > 0 && max(arr[j], arr[j-1]); j--){
					   exch(arr,j,j-1);
				   }
			   }
		   }
		   return arr;
	   }
	   
	   private void quicksort(Comparable[] lista, int inicio, int fin, TipoOrdenamiento orden)
	     {
	    	 // Trabajo en Clase
	    	 if(fin <= inicio) return;
	    	 int j = particion(lista, inicio, fin, orden);
	    	 quicksort(lista, inicio, j-1, orden);
	    	 quicksort(lista, j+1, fin, orden);
	     }
	   
	   private int particion(Comparable[] lista, int inicio, int fin, TipoOrdenamiento orden)
	    {
	    	// Trabajo en Clase
	    	if(orden.equals(TipoOrdenamiento.ASCENDENTE)){
	    		int i = inicio;
	    		int j = fin + 1;
	    		Comparable p = lista[inicio];
	    		while(true){
	    			while (less(lista[++i], p)){
	    				if (i == fin){
	    					break;
	    				}
	    			}
	    			while(less(p, lista[--j])){
	    				if(j == inicio){
	    					break;
	    				}
	    			}
	    			if(i >= j){
	    				break;
	    			}
	    			exch(lista, i, j);
	    		}
	    		exch(lista, inicio, j);
	    		return j;
	    	}
	    	else {
	    		int i = inicio;
	    		int j = fin + 1;
	    		Comparable p = lista[inicio];
	    		while(true){
	    			while (max(lista[++i], p)){
	    				if (i == fin){
	    					break;
	    				}
	    			}
	    			while(max(p, lista[--j])){
	    				if(j == inicio){
	    					break;
	    				}
	    			}
	    			if(i >= j){
	    				break;
	    			}
	    			exch(lista, i, j);
	    		}
	    		exch(lista, inicio, j);
	    		return j;
	    	}
	    	
	    	
	    }

	    private int eleccionPivote(int inicio, int fin)
	    {
	         /**
	           Este procedimiento realiza la elecci�n de un �ndice que corresponde al pivote res-
	           pecto al cual se realizar�  la partici�n de la lista. Se recomienda escoger el ele-
	           mento que se encuentra en la mitad, o de forma aleatoria entre los �ndices [inicio, fin).
	         **/
	    	// Trabajo en Clase
	         return 0;
	    }


	   
	   

}
