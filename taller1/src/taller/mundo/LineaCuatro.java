package taller.mundo;

import java.util.ArrayList;

public class LineaCuatro 
{
	/**
	 * Matriz que representa el estado actual del juego.
	 */
	private String[][] tablero;

	/**
	 * Lista que contiene a los jugadores
	 */
	private ArrayList<Jugador> jugadores;

	/**
	 * Nombre del jugador en turno
	 */
	private String atacante;

	/**
	 * Determina si el juego se terminó
	 */
	private boolean finJuego;

	/**
	 * El número del jugador en turno
	 */
	private int turno;
	/**
	 * El numero de fichas con las qque se gana el juego
	 */
	private int fichasVictoria;

	/**
	 * Crea una nueva instancia del juego
	 */
	public LineaCuatro(ArrayList <Jugador> pJugadores,int pFil, int pCol , int pFichasVictoria)
	{
		tablero = new String[pFil][pCol];
		for(int i=0;i<tablero.length;i++)
		{
			for(int j=0;j<tablero[0].length;j++)
			{
				tablero[i][j]="___";
			}

		}
		jugadores = pJugadores;
		finJuego = false;
		turno = 0;
		atacante = jugadores.get(turno).darNombre();
		fichasVictoria = pFichasVictoria;
	}
	/**
	 * Retorna el tablero del juego
	 * @return tablero
	 */
	public String[][] darTablero()
	{
		return tablero;
	}

	/**
	 * Retorna el jugador en turno
	 * @return atacante jugador en turno
	 */
	public String darAtacante()
	{
		return atacante;
	}
	/**
	 * Determina si el juego termina
	 * @return true si el juego se termino, false de lo contrario
	 */
	public boolean fin()
	{
		return finJuego;
	}

	/**
	 * Registra una jugada aleatoria
	 */
	public void registrarJugadaAleatoria()
	{
		//TODO
		int columnaAleatoria = (int) (Math.random()*tablero[0].length + 1);
		boolean jugada = registrarJugada(columnaAleatoria - 1);
		if(jugada == false){
			registrarJugadaAleatoria();
		}


	}

	/**
	 * Registra una jugada en el tablero
	 * @return true si la jugada se pudo realizar, false de lo contrario
	 */
	public boolean registrarJugada(int col)
	{
		//TODO
		int i = tablero.length -1;
		boolean insertado = false;
		Jugador jug = (Jugador) jugadores.get(turno);
		String simbolo = jug.darSimbolo();
		while(i >= 0 && !insertado){
			if(tablero[i][col] == "___"){
				tablero[i][col] = "_"+simbolo + "_";
				insertado = true;
				finJuego = terminar(i,col);
			}
			i--;
		}
		if(insertado == true){
			if(turno == (jugadores.size() - 1) ){
				turno = 0;
			}
			else{
				turno ++;
			}
			return true;
		}
		else{
			return false;
		}


	}

	/**
	 * Determina si una jugada causa el fin del juego
	 * @return true si la jugada termina el juego false de lo contrario
	 */
	public boolean terminar(int fil,int col)
	{
		//TODO
		boolean verificarAlCostado = terminoCostado(fil,col);
		if(verificarAlCostado == true){
			return true;
		}
		else{
			boolean verificarArribaAbajo = terminoArribaAbajo(fil,col);
			if(verificarArribaAbajo == true){
				return true;
			}
			else{
				boolean verificarDiagonalesArriba = terminarDiagonalesArriba(fil, col);
				if(verificarDiagonalesArriba == true){
					return true;
				}
				else{
					boolean verificarDiagonalesAbajo = terminarDiagonalesAbajo(fil,col);
					if(verificarDiagonalesAbajo == true){
						return true;
					}
					else{
						return false;
					}
				}
			}
		}
	}

	public boolean terminoCostado(int fil, int col){
		int fichasSeguidas = 1;
		String simboloAVer = tablero[fil][col];
		boolean noSeguido = false;
		for(int i = (col + 1); i < tablero[0].length && !noSeguido; i++){
			if(simboloAVer.equals(tablero[fil][i])){
				fichasSeguidas ++;
			}
			else{
				noSeguido = true;
			}
		}

		if(fichasSeguidas >= fichasVictoria){
			return true;
		}
		else{
			fichasSeguidas = 1;
			noSeguido = false;
			for(int i = (col - 1); i >= 0 && !noSeguido; i-- ){
				if(simboloAVer.equals(tablero[fil][i])){
					fichasSeguidas ++;
				}
				else{
					noSeguido = true;
				}
			}
			if(fichasSeguidas >= fichasVictoria){
				return true;
			}
			else{
				return false;
			}
		}


	}

	public boolean terminoArribaAbajo (int fil, int col){
		int fichasSeguidas = 1;
		String simboloAVer = tablero[fil][col];
		boolean noSeguido = false;
		for(int i = (fil + 1); i < tablero.length && !noSeguido; i++){
			if(simboloAVer.equals(tablero[i][col])){
				fichasSeguidas ++;
			}
			else{
				noSeguido = true;
			}
		}

		if(fichasSeguidas >= fichasVictoria){
			return true;
		}
		else{
			fichasSeguidas = 1;
			noSeguido = false;
			for(int i = (fil - 1); i >= 0 && !noSeguido; i-- ){
				if(simboloAVer.equals(tablero[i][col])){
					fichasSeguidas ++;
				}
				else{
					noSeguido = true;
				}
			}
			if(fichasSeguidas >= fichasVictoria){
				return true;
			}
			else{
				return false;
			}
		}

	}

	public boolean terminarDiagonalesArriba(int fil, int col){
		int fichasSeguidas = 1;
		String simboloAVer = tablero[fil][col];
		boolean noSeguido = false;
		int indiceFila = fil + 1;
		int indiceColu = col + 1;
		while(indiceFila < tablero.length && indiceColu < tablero[0].length && !noSeguido){
			if(simboloAVer.equals(tablero[indiceFila][indiceColu])){
				fichasSeguidas ++;
			}
			else{
				noSeguido = true;
			}
			indiceFila ++;
			indiceColu ++;
		}

		if(fichasSeguidas >= fichasVictoria){
			return true;
		}
		else{
			fichasSeguidas = 1;
			noSeguido = false;
			indiceFila = fil + 1;
			indiceColu = col - 1;
			while(indiceFila < tablero.length && indiceColu >= 0 && !noSeguido){
				if(simboloAVer.equals(tablero[indiceFila][indiceColu])){
					fichasSeguidas ++;
				}
				else{
					noSeguido = true;
				}
				indiceFila ++;
				indiceColu --;
			}
			if(fichasSeguidas >= fichasVictoria){
				return true;
			}
			else{
				return false;
			}
		}




	}

	public boolean terminarDiagonalesAbajo (int fil , int col){
		int fichasSeguidas = 1;
		String simboloAVer = tablero[fil][col];
		boolean noSeguido = false;
		int indiceFila = fil -1;
		int indiceColu = col + 1;
		while(indiceFila < tablero.length && indiceColu < tablero[0].length && !noSeguido){
			if(simboloAVer.equals(tablero[indiceFila][indiceColu])){
				fichasSeguidas ++;
			}
			else{
				noSeguido = true;
			}
			indiceFila --;
			indiceColu ++;
		}

		if(fichasSeguidas >= fichasVictoria){
			return true;
		}
		else{
			fichasSeguidas = 1;
			noSeguido = false;
			indiceFila = fil -1;
			indiceColu = col - 1;
			while(indiceFila < tablero.length && indiceColu >= 0 && !noSeguido){
				if(simboloAVer.equals(tablero[indiceFila][indiceColu])){
					fichasSeguidas ++;
				}
				else{
					noSeguido = true;
				}
				indiceFila --;
				indiceColu --;
			}
			if(fichasSeguidas >= fichasVictoria){
				return true;
			}
			else{
				return false;
			}
		}

	}




}
