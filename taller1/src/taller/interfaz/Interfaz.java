package taller.interfaz;

import java.util.ArrayList;
import java.util.Scanner;

import taller.mundo.Jugador;
import taller.mundo.LineaCuatro;

public class Interfaz 
{
	/**
	 * Juego actual
	 */
	private LineaCuatro juego;

	/**
	 * EscÃ¡ner de texto ingresado por el usuario en la consola
	 */
	private Scanner sc;

	/**
	 * Crea una nueva instancia de la clase de interacciÃ³n del usuario con la consola
	 */
	public Interfaz()
	{
		sc= new Scanner (System.in);
		while (true)
		{
			imprimirMenu();
			try
			{
				int opt=Integer.parseInt(sc.next());
				if(opt==1)
				{
					empezarJuego();
				}
				else if(opt==2)
				{
					empezarJuegoMaquina();
				}
				else if(opt==3)
				{
					System.out.println("Â¡Vuelva pronto!");
					break;
				}
				else
				{
					System.out.println("Comando invÃ¡lido");
					continue;
				}
			}
			catch (NumberFormatException e)
			{
				System.out.println("Comando invÃ¡lido");
				continue;
			}


		}
	}
	/**
	 * Imprime el menÃº principal
	 */
	public void imprimirMenu()
	{
		System.out.println("------------------------LÃ�NEA CUATRO------------------------");
		System.out.println("-MenÃº principal-");
		System.out.println("Ingrese un comando:");
		System.out.println("1. Empezar juego con amigos");
		System.out.println("2. Empezar juego vs mÃ¡quina");
		System.out.println("3. Salir");		
	}

	/**
	 * Crea un nuevo juego
	 */
	public void empezarJuego()
	{
		//TODO
		System.out.println("Introduzca el numero de jugadores (Deben ser 2 o más)");
		int numJug = Integer.valueOf(sc.next());
		ArrayList jugadores = new ArrayList();
		for(int i = 0; i < numJug; i++){
			System.out.println("Ingrese el nombre del jugador " + (i+1));
			String nombre = sc.next();
			System.out.println("Ingrese el simbolo del jugador " + (i+1));
			String simbolo = sc.next();
			boolean valido = validarSimbolo(simbolo);
			while (valido == false){
				System.out.println("Solo puede ingresar un caracter, ingrese el simbolo que desee usar");
				simbolo = sc.next();
				valido = validarSimbolo(simbolo);
			}
			Jugador nuevo = new Jugador(nombre, simbolo);
			jugadores.add(nuevo);

		}

		System.out.println("Ingrese el número de columnas para el tablero");
		int col = Integer.valueOf(sc.next());
		System.out.println("Ingrese el numero de filas para el tablero");
		int fil = Integer.valueOf(sc.next());
		System.out.println("Ingrese el numero de fichas con las que se gana el juego");
		int fichasV = Integer.valueOf(sc.next());

		juego = new LineaCuatro(jugadores,fil,col,fichasV);
		juego();
	}

	/**
	 * Modera el juego entre jugadores
	 */
	public void juego()
	{
		//TODO
		imprimirTablero();
		boolean terminado = juego.fin();
		while (terminado == false){
			System.out.println("Ingrese la columna de la siguiente jugada (La primera columna es 0)");
			int col = Integer.valueOf(sc.next());
			try{
				boolean jugada =	juego.registrarJugada(col);
				if(jugada == false){
					System.out.println("No fue posible realizar la jugada, vuelva a intentarlo");
				}
				else{
					imprimirTablero();
					terminado = juego.fin();
				}
			}
			catch(Exception e){
				System.out.println("Comando invalido, vuelva a intentarlo");
				imprimirTablero();
			}

		}


	}
	/**
	 * Empieza el juego contra la mÃ¡quina
	 */
	public void empezarJuegoMaquina()
	{
		//TODO
		System.out.println("Ingrese el nombre del jugador");
		String nombre = sc.next();
		System.out.println("Escoja el símbolo que desee usar");
		String simbolo = sc.next();
		boolean valido = validarSimbolo(simbolo);
		while (valido == false){
			System.out.println("Solo puede ingresar un caracter, ingrese el simbolo que desee usar");
			simbolo = sc.next();
			valido = validarSimbolo(simbolo);
		}
		Jugador nuevo = new Jugador (nombre, simbolo);
		Jugador maquina = new Jugador ("Maquina", "X");
		ArrayList jugadores = new ArrayList();
		jugadores.add(nuevo);
		jugadores.add(maquina);

		System.out.println("Ingrese el numero de columnas para el tablero");
		int col = Integer.valueOf(sc.next());
		System.out.println("Ingrese el numero de filas para el tablero");
		int fil = Integer.valueOf(sc.next());
		System.out.println("Ingrese el numero de fichas con las que se gana el juego");
		int numVic = Integer.valueOf(sc.next());
		juego = new LineaCuatro(jugadores, fil, col, numVic);

		juegoMaquina();


	}
	/**
	 * Modera el juego contra la mÃ¡quina
	 */
	public void juegoMaquina()
	{
		//TODO
		boolean terminado = juego.fin();
		while (terminado == false){
			System.out.println("Ingrese la columna de la siguiente jugada (La primera columna es 0)");
			int columna = Integer.valueOf(sc.next());
			try{
				boolean jugada = juego.registrarJugada(columna);
				if(jugada == false){
					System.out.println("no fue posible hacer la jugada, reintentar");
				}
				else{
					terminado = juego.fin();
					imprimirTablero();
					System.out.println();
					System.out.println();
					if(terminado == false){
						juego.registrarJugadaAleatoria();
						imprimirTablero();
					}
					terminado = juego.fin();

				}
			}
			catch(Exception e){
				System.out.println("Comando invalido, vuelva a intentarlo");
				imprimirTablero();
			}


		}
	}

	/**
	 * Imprime el estado actual del juego
	 */
	public void imprimirTablero()
	{
		//TODO
		String tabla[][] = juego.darTablero();
		for(int i = 0; i < tabla.length; i++){
			for(int j = 0; j < tabla[0].length; j++){
				System.out.print("|" + tabla[i][j] + "|");
			}
			System.out.println();
		}
	}
	
	public boolean validarSimbolo(String pSimbolo){
		if(pSimbolo.length() == 1){
			return true;
		}
		else{
			return false;
		}
	}
}
